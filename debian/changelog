quickflux (1.1.3+git20201110.2a37acf-4) unstable; urgency=medium

  * Upload source-only as is.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 02 Dec 2024 12:55:55 +0100

quickflux (1.1.3+git20201110.2a37acf-3) unstable; urgency=medium

  [ Guido Berhoerster ]
  * debian/control:
    + Drop shared library bin:pkgs, add QML module bin:pkg instead.
  * debian/patches:
    + Replace 1001_build-shared-lib.patch with 1001_build-qml-module.patch.

  [ Mike Gabriel ]
  * debian/watch: Drop uupdate call.
  * debian/quickflux-doc.install:
    + Install docs now into /usr/share/doc/qml-module-quickflux.
  * debian/quickflux-doc.doc-base:
    + Add file.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 02 Dec 2024 12:27:23 +0100

quickflux (1.1.3+git20201110.2a37acf-2) unstable; urgency=medium

  * debian/patches:
    + Improve patch as suggested by @OPNA2608.
  * debian/rules:
    + Ignore failures when removing installed example files during bin:arch-only
      builds. (Closes: #1068723).
    + Adjust CMake build option (quickflux_BUILD_SHARED -> BUILD_SHARED_LIBS).

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 10 Apr 2024 23:17:41 +0200

quickflux (1.1.3+git20201110.2a37acf-1) unstable; urgency=low

  * Initial upload to Debian. (Closes: #1068261).

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 02 Apr 2024 22:45:59 +0200
